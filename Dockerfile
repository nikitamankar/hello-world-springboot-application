FROM openjdk:8-jdk-alpine

COPY build/libs/hello-world-0.1.0.jar docker-hello-world.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","/docker-hello-world.jar"]
